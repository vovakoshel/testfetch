import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Sidebar from "./components/Sidebar.js";
import Articles from "./components/Articles";
import EditArticle from "./components/EditArticle";
import AddArticle from "./components/AddArticle";
import PublishedArticles from "./components/PublishedArticles";
import DraftArticles from "./components/DraftArticles";
import HiddenArticles from "./components/HiddenArticles";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
const path = "89.223.92.96:8080";
class App extends Component {
  state = {
    articles: []
  };

  fetchAllArticles = async () => {
    console.log(path);
    const articles = await fetch(`http://${path}/Blog/ViewAllArticles`).then(
      response => response.json()
    );
    return articles.reverse();
  };

  async componentDidMount() {
    const articles = await this.fetchAllArticles();
    this.setState({
      articles: articles
    });
  }

  deleteArticle = id => {
    this.setState({
      articles: this.state.articles.filter(article => article.id !== id)
    });
    fetch(`http://${path}/Blog/DeleteArticle/${id}`, {
      method: "DELETE"
    });
  };
  // add article to publish
  addArticle = async (title, body) => {
    await fetch(`http://${path}/Blog/AddArticle`, {
      method: "POST",
      body: JSON.stringify({
        title: title,
        body: body,
        status: 2
      }),
      headers: {
        "Content-type": "application/json; charset=UTF-8"
      }
    });

    const articles = await this.fetchAllArticles();

    this.setState({
      articles: articles
    });
  };
  // add article to draft
  addArticleToDraft = async (title, body) => {
    await fetch(`http://${path}/Blog/AddArticle`, {
      method: "POST",
      body: JSON.stringify({
        title: title,
        body: body,
        status: 1
      }),
      headers: {
        "Content-type": "application/json; charset=UTF-8"
      }
    });

    const articles = await this.fetchAllArticles();

    this.setState({
      articles: articles
    });
  };
  // Edit article
  editArticle = async (body, title, id, status) => {
    await fetch(`http://${path}/Blog/UpdateArticle/${id}`, {
      method: "PUT",
      body: JSON.stringify({
        title: title,
        body: body,
        status: status
      }),
      headers: {
        "Content-type": "application/json; charset=UTF-8"
      }
    });

    const articles = await this.fetchAllArticles();

    this.setState({
      articles: articles
    });
  };

  // change article status
  changeArticleStatus = async (body, title, id) => {
    console.log("ty gde bleac");
    await fetch(`http://${path}/Blog/UpdateArticle/${id}`, {
      method: "PUT",
      body: JSON.stringify({
        title: title,
        body: body,
        status: 2
      }),
      headers: {
        "Content-type": "application/json; charset=UTF-8"
      }
    });

    const articles = await this.fetchAllArticles();

    this.setState({
      articles: articles
    });
  };

  changeStatusToHidden = async (id, title, body) => {
    await fetch(`http://${path}/Blog/UpdateArticle/${id}`, {
      method: "PUT",
      body: JSON.stringify({
        title: title,
        body: body,
        status: 0
      }),
      headers: {
        "Content-type": "application/json; charset=UTF-8"
      }
    });

    const articles = await this.fetchAllArticles();

    this.setState({
      articles: articles
    });
  };

  changeStatusToShown = async (id, title, body) => {
    await fetch(`http://${path}/Blog/UpdateArticle/${id}`, {
      method: "PUT",
      body: JSON.stringify({
        title: title,
        body: body,
        status: 2
      }),
      headers: {
        "Content-type": "application/json; charset=UTF-8"
      }
    });

    const articles = await this.fetchAllArticles();

    this.setState({
      articles: articles
    });
  };

  render() {
    const { articles } = this.state;

    return (
      <Router>
        <div className="App">
          <Sidebar />
          <div className="views">
            <div className="container">
              <Switch>
                <Route
                  {...this.props}
                  render={() => (
                    <Articles
                      articles={articles}
                      deleteArticle={this.deleteArticle}
                      changeStatusToHidden={this.changeStatusToHidden}
                      changeStatusToShown={this.changeStatusToShown}
                    />
                  )}
                  exact
                  path="/"
                />

                <Route
                  path="/article/add"
                  render={routeProps => (
                    <AddArticle
                      {...routeProps}
                      addArticle={this.addArticle}
                      addArticleToDraft={this.addArticleToDraft}
                    />
                  )}
                />

                <Route
                  path="/article/edit/:id"
                  render={routeProps => (
                    <EditArticle
                      {...routeProps}
                      editArticle={this.editArticle}
                      changeArticleStatus={this.changeArticleStatus}
                    />
                  )}
                />
                <Route
                  path="/publishedArticles"
                  render={routeProps => (
                    <PublishedArticles
                      {...routeProps}
                      articles={articles}
                      deleteArticle={this.deleteArticle}
                      changeStatusToHidden={this.changeStatusToHidden}
                    />
                  )}
                />
                <Route
                  path="/draftArticles"
                  render={routeProps => (
                    <DraftArticles
                      {...routeProps}
                      articles={articles}
                      deleteArticle={this.deleteArticle}
                    />
                  )}
                />
                <Route
                  path="/hiddenArticles"
                  render={routeProps => (
                    <HiddenArticles
                      {...routeProps}
                      articles={articles}
                      deleteArticle={this.deleteArticle}
                      changeStatusToShown={this.changeStatusToShown}
                    />
                  )}
                />
              </Switch>
            </div>
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
