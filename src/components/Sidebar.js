import React, { Component } from "react";
import { Link } from "react-router-dom";
class Sidebar extends Component {
  state = {
    extended: false
  };
  showSubmenu = () => {
    this.setState({
      extended: !this.state.extended
    });
  };
  filterArticles = () => {
    this.props.filterArticles();
  };
  render() {
    return (
      <div className="sidebar">
        <div className="logo">
          <Link to="/">
            <span>
              <i className="fas fa-poo mr-2" style={{ color: "#764d2b" }} />
              <span className="">CapitalLetter</span>
            </span>
          </Link>
        </div>
        <div className="search">
          <input type="text" placeholder="search article..." />
        </div>

        <div className="sidebar-menu">
          <ul>
            <li>
              <Link to="/article/add">
                <span>
                  <i className="fas fa-plus" /> Create article
                </span>
              </Link>
            </li>
            <li>
              <span onClick={this.showSubmenu} style={{ cursor: "pointer" }}>
                <i className="far fa-clipboard" /> Articles{" "}
                <i className="fas fa-sort-down" />
              </span>
            </li>
            {this.state.extended ? (
              <ul>
                <li>
                  <Link to="/">
                    <span> - All</span>
                  </Link>
                </li>

                <li>
                  <Link to="/publishedArticles">
                    <span> - Published</span>
                  </Link>
                </li>

                <li>
                  <Link to="/draftArticles">
                    <span> - UnPublished</span>
                  </Link>
                </li>

                <li>
                  <Link to="/hiddenArticles">
                    <span> - Hidden</span>
                  </Link>
                </li>
              </ul>
            ) : null}
          </ul>
        </div>
      </div>
    );
  }
}

export default Sidebar;
