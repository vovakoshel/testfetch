import React, { Component } from "react";

class ItemsList extends Component {
  showArticle = id => {
    this.props.showArticle(id);
  };
  render() {
    const { id, title } = this.props.article;
    return (
      <div>
        <span
          onClick={this.showArticle.bind(this, id)}
          style={{ cursor: "pointer" }}
          dangerouslySetInnerHTML={{ __html: title }}
        />
      </div>
    );
  }
}
export default ItemsList;
