import React, { Component } from "react";
import { Link } from "react-router-dom";
class Article extends Component {
  state = {
    extended: false
  };

  showBody = () => {
    this.setState({
      extended: !this.state.extended
    });
  };

  onDeleteClick = id => this.props.deleteArticle(id);

  changeStatusToHidden = (id, title, body) =>
    this.props.changeStatusToHidden(id, title, body);

  changeStatusToShown = (id, title, body) =>
    this.props.changeStatusToShown(id, title, body);

  render() {
    const { extended } = this.state;
    const { id, title, body, status } = this.props.article;
    return (
      <div>
        <div className="article ">
          <div className="article-title">
            <div>
              <div className="badge badge-danger">
                <span style={{ fontSize: "17px" }}>
                  {status === 0 ? "H" : status === 1 ? "U" : "P"}
                </span>
              </div>
            </div>
            <div className="article-caption">
              <span
                className="font-editor"
                onClick={this.showBody}
                dangerouslySetInnerHTML={{ __html: title }}
              />
            </div>
            <div className="article-settings">
              <i
                title="Delete article"
                onClick={this.onDeleteClick.bind(this, id)}
                className="fas fa-trash-alt "
              />
              <Link to={`article/edit/${id}`}>
                <i className="fas fa-pencil-alt " title="Edit article" />
              </Link>

              {status === 2 ? (
                <i
                  className="fas fa-eye-slash"
                  title="Hidden article"
                  onClick={this.changeStatusToHidden.bind(
                    this,
                    id,
                    title,
                    body
                  )}
                />
              ) : null}
              {status === 0 ? (
                <i
                  onClick={this.changeStatusToShown.bind(this, id, title, body)}
                  className="fas fa-eye"
                  title="show article"
                />
              ) : null}
            </div>
          </div>
          <div className="article-body">
            {extended ? (
              <span dangerouslySetInnerHTML={{ __html: body }} />
            ) : null}
          </div>
        </div>
      </div>
    );
  }
}

export default Article;
