import React, { Component } from "react";
import Article from "./Article";
class DraftArticles extends Component {
  render() {
    const { articles } = this.props;
    const draftArticles = articles.filter(article => article.status === 1);
    return (
      <div>
        <h2 className="fontheader mb-5 mt-3">Your UnPublished Articles</h2>
        {draftArticles.map(article => (
          <Article
            key={article.id}
            article={article}
            deleteArticle={this.props.deleteArticle}
          />
        ))}
      </div>
    );
  }
}
export default DraftArticles;
