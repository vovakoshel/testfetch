import React, { Component } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
class Header extends Component {
  render() {
    const { title } = this.props;
    return (
      <nav className="navbar bg-dark">
        <a href="/" className="navbar-brand ">
          {title}
        </a>

        <Link to="/article/add" className="nav-link">
          <i className="fas fa-plus" /> Add
        </Link>
        <Link to="/articleslist" className="nav-link">
          <i className="fas fa-list" /> Articles List
        </Link>
      </nav>
    );
  }
}

Header.defaultProps = {
  title: "My-app"
};

Header.propTypes = {
  title: PropTypes.string.isRequired
};

export default Header;
