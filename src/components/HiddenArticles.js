import React, { Component } from "react";
import Article from "./Article";
class HiddenArticles extends Component {
  render() {
    const { articles } = this.props;
    const hiddenArticles = articles.filter(article => article.status === 0);
    return (
      <div>
        <h2 className="fontheader mb-5 mt-3">Your Hidden Articles</h2>
        {hiddenArticles.map(article => (
          <Article
            key={article.id}
            article={article}
            deleteArticle={this.props.deleteArticle}
            changeStatusToShown={this.props.changeStatusToShown}
          />
        ))}
      </div>
    );
  }
}
export default HiddenArticles;
