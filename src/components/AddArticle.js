import Editor from "./Editor";
import "react-quill/dist/quill.snow.css";
import "react-quill/dist/quill.bubble.css";
import React, { Component } from "react";

class AddArticle extends Component {
  state = { title: "", body: "" };

  handleChangeTitle = value => {
    this.setState({ title: value });
  };
  handleChangeBody = value => {
    this.setState({ body: value });
  };

  addArticle = (title, body) => {
    this.props.addArticle(title, body);
    this.setState({
      body: "",
      title: ""
    });
    this.props.history.push("/");
  };

  addArticleToDraft = (title, body) => {
    this.props.addArticleToDraft(title, body);
    this.setState({
      body: "",
      title: ""
    });
    this.props.history.push("/");
  };

  render() {
    const { title, body } = this.state;
    return (
      <div>
        <h2 className="fontheader mb-5 mt-3">Create your article</h2>
        <br />
        <Editor
          valueTitle={this.state.title}
          valueBody={this.state.body}
          handleChangeTitle={this.handleChangeTitle}
          handleChangeBody={this.handleChangeBody}
        />

        <span
          onClick={this.addArticle.bind(this, title, body)}
          style={{ float: "right", cursor: "pointer", fontSize: 25 }}
        >
          <i className="far fa-clipboard" /> Publish
        </span>
        <span
          onClick={this.addArticleToDraft.bind(this, title, body)}
          style={{
            float: "right",
            cursor: "pointer",
            fontSize: 25,
            marginRight: 40
          }}
        >
          <i className="far fa-save" /> Save
        </span>
      </div>
    );
  }
}

export default AddArticle;
