import React, { Component } from "react";
import Article from "./Article";

class PublishedArticles extends Component {
  render() {
    const { articles } = this.props;
    const publishedArticles = articles.filter(article => article.status === 2);
    return (
      <div>
        <h2 className="fontheader mb-5 mt-3">Your Published Articles</h2>
        {publishedArticles.map(article => (
          <Article
            key={article.id}
            article={article}
            deleteArticle={this.props.deleteArticle}
            changeStatusToHidden={this.props.changeStatusToHidden}
          />
        ))}
      </div>
    );
  }
}
export default PublishedArticles;
