import Editor from "./Editor";
import "react-quill/dist/quill.snow.css";
import "react-quill/dist/quill.bubble.css";
import React, { Component } from "react";
const path = "89.223.92.96:8080";

class EditArticle extends Component {
  state = {
    title: "",
    body: "",
    status: null
  };

  async componentDidMount() {
    const { id } = this.props.match.params;
    const article = await fetch(
      `http://${path}/Blog/ViewArticleById/${id}`
    ).then(response => response.json());
    this.setState({
      body: article.body,
      title: article.title,
      status: article.status
    });
  }

  handleChangeBody = value => {
    this.setState({ body: value });
  };
  handleChangeTitle = value => {
    this.setState({ title: value });
  };

  editArticle = (body, title, id, status) => {
    this.props.editArticle(body, title, id, status);

    this.props.history.push("/");
  };

  changeArticleStatus = (body, title, id) => {
    this.props.changeArticleStatus(body, title, id);
    this.props.history.push("/");
  };

  render() {
    const { title, body, status } = this.state;
    const { id } = this.props.match.params;
    return (
      <div>
        <h2 className="fontheader mb-5 mt-3">Edit your article</h2>
        <Editor
          valueTitle={title}
          valueBody={body}
          handleChangeTitle={this.handleChangeTitle}
          handleChangeBody={this.handleChangeBody}
        />

        <span
          onClick={this.editArticle.bind(this, body, title, id, status)}
          style={{ float: "right", cursor: "pointer", fontSize: 25 }}
        >
          <i className="far fa-edit" /> Edit
        </span>

        {status !== 2 ? (
          <span
            onClick={this.changeArticleStatus.bind(this, body, title, id)}
            style={{
              float: "right",
              cursor: "pointer",
              fontSize: 25,
              marginRight: "20px"
            }}
          >
            <i className="far fa-clipboard" /> Publish
          </span>
        ) : null}
      </div>
    );
  }
}

export default EditArticle;
