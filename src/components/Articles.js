import React, { Component } from "react";
import Article from "./Article";

class Articles extends Component {
  render() {
    const { articles } = this.props;
    return (
      <React.Fragment>
        <h2 className="fontheader mb-5 mt-3">
          All Your Articles - {this.props.articles.length}
        </h2>

        {articles.map(article => (
          <Article
            key={article.id}
            article={article}
            deleteArticle={this.props.deleteArticle}
            changeStatusToHidden={this.props.changeStatusToHidden}
            changeStatusToShown={this.props.changeStatusToShown}
          />
        ))}
      </React.Fragment>
    );
  }
}

export default Articles;
