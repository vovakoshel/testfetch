package com.ourcompany.blog.dao.daofactory;

import com.ourcompany.blog.dao.impl.SQLArticleDAO;

public class DAOFactory {

	private DAOFactory() {
	}

	private final static DAOFactory instance = new DAOFactory();

	private final SQLArticleDAO sqlArticleDAO = new SQLArticleDAO();

	public static DAOFactory getDAOFactory() {
		return instance;
	}

	public SQLArticleDAO getSQLArticleDAO() {
		return sqlArticleDAO;
	}

}
