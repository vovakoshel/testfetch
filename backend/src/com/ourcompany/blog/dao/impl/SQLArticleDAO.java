package com.ourcompany.blog.dao.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.ourcompany.blog.dao.ArticleDAO;
import com.ourcompany.blog.entity.Article;
import com.ourcompany.blog.entity.Storage;

public class SQLArticleDAO implements ArticleDAO {
	private Configuration configuration = new Configuration().configure();
	private SessionFactory sessionFactory = configuration.buildSessionFactory();


	@Override
	public void addArticle(String title, String description, String text, int status) {
		try {
			Session session = sessionFactory.openSession();
			Article article = new Article();
			article.setTitle(title);
			article.setDescription(description);
			article.setBody(text);
			article.setStatus(status);
			session.save(article);
			Transaction transaction = session.beginTransaction();
			transaction.commit();
			System.out.println("\n\n Article was added \n");

		} catch (HibernateException e) {
			System.out.println(e.getMessage());
			System.out.println("error");
			e.printStackTrace();
		}

	}

	@Override
	public List<Article> viewAllArticles() {
		try {
			Session session = sessionFactory.openSession();
			Query query = session.createQuery("FROM Article");
			List<Article> list = query.list();
			return list;
		} catch (HibernateException e) {
			System.out.println(e.getMessage());
			System.out.println("error");
			Article art = new Article();			
			art.setTitle("Error!");art.setBody(e.getMessage()); art.setStatus(1);
			Storage.list.add(art);
			return Storage.list;
		}
		

	}

	@Override
	public void deleteByID(int id) {
		try {
			Session session = sessionFactory.openSession();
			Query query = session.createQuery("FROM Article where ARTICLE_ID = " + id);
			List<Article> list = query.list();
			Article art = list.get(0);
			session.delete(art);
			Transaction transaction = session.beginTransaction();
			transaction.commit();
		} catch (HibernateException e) {
			System.out.println(e.getMessage());
			System.out.println("error");
			e.printStackTrace();
		}

	}

	@Override
	public Article viewArticleByID(int id) {
		try {
			Session session = sessionFactory.openSession();
			Query query = session.createQuery("FROM Article where ARTICLE_ID = " + id);
			List<Article> list = query.list();
			Article art = list.get(0);
			return art;
		} catch (HibernateException e) {
			System.out.println(e.getMessage());
			System.out.println("error");
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void UpdateArticle(Article art) {
		try {
			Session session = sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			session.update(art);
			transaction.commit();
		} catch (HibernateException e) {
			System.out.println(e.getMessage());
			System.out.println("error");
			e.printStackTrace();
		}

	}

}
