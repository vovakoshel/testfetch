package com.ourcompany.blog.dao;

import java.util.List;

import com.ourcompany.blog.entity.Article;

public interface ArticleDAO {
	
	void addArticle(String title, String description, String text, int status);
	public List<Article> viewAllArticles();
	public void deleteByID(int id);
	public Article viewArticleByID(int id);
	public void UpdateArticle(Article art);

}
