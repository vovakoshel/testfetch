package com.ourcompany.blog.controller.impl;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ourcompany.blog.controller.GSONServlet;
import com.ourcompany.blog.dao.daofactory.DAOFactory;
import com.ourcompany.blog.dao.impl.SQLArticleDAO;

public class DeleteArticle extends GSONServlet {
	
	private int id;
	@Override
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		id =  Integer.parseInt(request.getRequestURI().substring(20,request.getRequestURI().length()));
		System.out.println(id);
		SQLArticleDAO sad = DAOFactory.getDAOFactory().getSQLArticleDAO();
		System.out.println("Delete ID : "+id);
		sad.deleteByID(id);

	}

}
