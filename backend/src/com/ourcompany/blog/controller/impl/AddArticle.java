package com.ourcompany.blog.controller.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.ourcompany.blog.controller.GSONServlet;
import com.ourcompany.blog.dao.daofactory.DAOFactory;
import com.ourcompany.blog.dao.impl.SQLArticleDAO;
import com.ourcompany.blog.entity.Article;

public class AddArticle extends GSONServlet {

	private String str;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try (BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream(),StandardCharsets.UTF_8))) {
			str = br.readLine();
			//System.out.println("Входная строка: " + str);
			Article article = getGson().fromJson(str, Article.class);
			SQLArticleDAO articleDAO = DAOFactory.getDAOFactory().getSQLArticleDAO();
			articleDAO.addArticle(article.getTitle(), article.getTitle(), article.getBody(), article.getStatus());
		}
	}
}
