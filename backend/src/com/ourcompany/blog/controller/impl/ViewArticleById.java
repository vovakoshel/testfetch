package com.ourcompany.blog.controller.impl;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ourcompany.blog.controller.GSONServlet;
import com.ourcompany.blog.dao.daofactory.DAOFactory;
import com.ourcompany.blog.dao.impl.SQLArticleDAO;
import com.ourcompany.blog.entity.Article;

public class ViewArticleById extends GSONServlet{
	
	private int id;
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/plain; charset=utf-8");
		id =  Integer.parseInt(request.getRequestURI().substring(22,request.getRequestURI().length()));
		SQLArticleDAO sad = DAOFactory.getDAOFactory().getSQLArticleDAO();
		Article art = sad.viewArticleByID(id);
		System.out.println(getGson().toJson(art));
		response.getWriter().write(getGson().toJson(art));

	}
}
