package com.ourcompany.blog.controller.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ourcompany.blog.controller.GSONServlet;
import com.ourcompany.blog.dao.daofactory.DAOFactory;
import com.ourcompany.blog.dao.impl.SQLArticleDAO;
import com.ourcompany.blog.entity.Article;

public class UpdateArticle extends GSONServlet {

	public UpdateArticle() {
		super();
	}

	private int id;
	private String str;

	@Override
	protected void doPut(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		id = Integer.parseInt(request.getRequestURI().substring(20, request.getRequestURI().length()));
		SQLArticleDAO sad = DAOFactory.getDAOFactory().getSQLArticleDAO();
		try (BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream(),"UTF-8"))) {
			str = br.readLine();
			Article art = getGson().fromJson(str, Article.class);
			art.setId(id);
			System.out.println(art.toString());
			sad.UpdateArticle(art);
		}
	}
}
