package com.ourcompany.blog.controller.impl;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import com.google.gson.Gson;
import com.ourcompany.blog.controller.GSONServlet;
import com.ourcompany.blog.dao.daofactory.DAOFactory;
import com.ourcompany.blog.dao.impl.SQLArticleDAO;
import com.ourcompany.blog.entity.Storage;

public class ViewAllArticles extends GSONServlet{
	@Override
	
	protected void doGet(HttpServletRequest request, javax.servlet.http.HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println();
		response.setContentType("text/plain; charset=utf-8");
		System.out.println(response.getContentType());
		System.out.println();
		SQLArticleDAO sad = DAOFactory.getDAOFactory().getSQLArticleDAO();
		Storage.list = sad.viewAllArticles();
		response.getWriter().write(getGson().toJson(Storage.list, getType()));
		Storage.list.clear();
	}
	
}
