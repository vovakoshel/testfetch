package com.ourcompany.blog.controller;

import java.lang.reflect.Type;
import java.util.List;
import javax.servlet.http.HttpServlet;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ourcompany.blog.entity.Article;

/**
 * Servlet implementation class GSONServlet
 */
public class GSONServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public GSONServlet() {
		super();
	}

	private final static Gson gson = new Gson();
	private final static Type type = new TypeToken<List>() {
	}.getType();

	public Gson getGson() {
		return gson;
	}

	public Type getType() {
		return type;
	}

	

}
